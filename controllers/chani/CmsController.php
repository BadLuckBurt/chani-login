<?php

	namespace Login\Controllers\Chani;

	use \Core\Controllers\Chani\CmsController AS Controller;
	use \User\Models\Chani\CmsUser;
	use \Phalcon\Security AS Security;

	class CmsController extends Controller {

		public function beforeExecuteRoute() {
			//Overwrite the default beforeExecuteRoute to allow access to the login page
		}

		/**
		 * Redirects the user if logged in or presents the login form when the user is not logged in
		 */
		public function indexAction() {
			if($this->userIsLoggedIn() == true) {
				$this->response->redirect('/chani/page/');
			} else {
				$oTranslator = $this->_getTranslation(__NAMESPACE__);
				$sHead = $this->renderHead();
				$aParams = array(
					't' => $oTranslator,
					'loginError' => $this->session->get('loginError',0)
				);
				$sBody = $this->view->render('chani/form',$aParams);
				$sHTML = $this->renderHTML('Chani Login',$sHead, $sBody, true);
				echo($sHTML);
			}
		}

		/**
		 * Process the submitted user login
		 */
		public function processAction() {
			//Retrieve POST data
			$sUserName = $this->request->getPost('sUserName',null,'dummy_value');
			$sPassword = $this->request->getPost('sPassword',null,'dummy_value');

			//Prepare query conditions
			$sConditions = 'sUserName = :sUserName:';
			$aConditions = array(
				$sConditions,
				'bind' => array(
					'sUserName' => $sUserName
				)
			);

			//Retrieve user record that matches the username
			$oUser = CmsUser::findFirst($aConditions);
			//If the user record has been found, check the password hash
			//Set user to false if no match is found
			if($oUser) {
				$oSecurity = new Security();
				if($oSecurity->checkHash($sPassword,$oUser->sPassword) == false) {
					$oUser = false;
				}
			}

			//User has not been found, set errors and redirect to login screen
			if($oUser === false) {
				//Login failed
				$this->session->set('loginSuccess', false);
				$this->session->set('userId', null);
				$this->session->set('loginError',1);
				$this->response->redirect('chani/login/');
			//Login successful: Store login data in session and redirect to Page overview
			} else {
				$this->session->set('loginSuccess', true);
				$this->session->set('userId', $oUser->id);
				//Login successful
				$this->response->redirect('chani/page/');

			}

		}

		//Log out user
		public function logoutAction() {
			$this->session->set('loginSuccess', false);
			$this->session->set('userId', null);
			$this->response->redirect('chani/login/');
		}

		public function _getTranslation($sNameSpace = '') {
			if($sNameSpace == '') {
				$sNameSpace = __NAMESPACE__;
			}
			return parent::_getTranslation($sNameSpace);
		}

	}