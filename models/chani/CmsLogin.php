<?php

	namespace Login\Models\Chani;

	use \Core\Models\Chani\CmsBlueprint;

	class CmsLogin extends CmsBlueprint {

		public $_model ='CmsLogin';

		public function getSource() {
			return 'login';
		}

		public function initialize() {
			$sClass = get_class($this);
			parent::initialize();

		}

	}