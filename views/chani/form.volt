<div class="dashboard-login">
    <form action="{{ url('chani/login/process/') }}" method="post">
        <h1>
            <img src="{{url('public/modules/page/cms/media/chaniMini.png')}}" title="Chani CMS" />
            Chani CMS
        </h1>
        <div class="uniForm">
            {% if loginError > 0 %}
                <div class="col">
                    <div class="oneCol">
                        <p>{{t._('loginError'~loginError)}}</p>
                    </div>
                </div>
            {% endif %}
            <div class="col">
                <div class="oneCol">
                    <label for="sUserName">{{t._('sUserName')}}</label>
                    {{ text_field('sUserName','size':255) }}
                </div>
            </div>
            <div class="col">
                <div class="oneCol">
                    <label for="sPassword">{{t._('sPassword')}}</label>
                    {{ password_field('sPassword') }}
                </div>
            </div>
            <div class="buttons">
                <button class="save" name="sSubmit" class="right add">{{t._('sSubmit')}}</button>
            </div>
        </div>
        <div class="clear"></div>
    </form>
</div>