<div class="dashboard-login">
    <form action="<?php echo $this->url->get('chani/login/process/'); ?>" method="post">
        <h1>
            <img src="<?php echo $this->url->get('public/modules/page/cms/media/chaniMini.png'); ?>" title="Chani CMS" />
            Chani CMS
        </h1>
        <div class="uniForm">
            <?php if ($loginError > 0) { ?>
                <div class="col">
                    <div class="oneCol">
                        <p><?php echo $t->_('loginError' . $loginError); ?></p>
                    </div>
                </div>
            <?php } ?>
            <div class="col">
                <div class="oneCol">
                    <label for="sUserName"><?php echo $t->_('sUserName'); ?></label>
                    <?php echo $this->tag->textField(array('sUserName', 'size' => 255)); ?>
                </div>
            </div>
            <div class="col">
                <div class="oneCol">
                    <label for="sPassword"><?php echo $t->_('sPassword'); ?></label>
                    <?php echo $this->tag->passwordField(array('sPassword')); ?>
                </div>
            </div>
            <div class="buttons">
                <button class="save" name="sSubmit" class="right add"><?php echo $t->_('sSubmit'); ?></button>
            </div>
        </div>
        <div class="clear"></div>
    </form>
</div>