<?php

	$aMessages = array(
		'module' => 'Login',
		'sUserName' => 'Gebruikersnaam',
		'sPassword' => 'Wachtwoord',
		'sSubmit' => 'Log in',
		'loginError1' => 'Je hebt een verkeerde gebruikersnaam of een verkeerd wachtwoord opgegeven.'
	);