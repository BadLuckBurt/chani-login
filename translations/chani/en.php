<?php

	$aMessages = array(
		'module' => 'Login',
		'sUserName' => 'Username',
		'sPassword' => 'Password',
		'sSubmit' => 'Log in',
		'loginError1' => 'You\'ve entered an incorrect username or password.'
	);